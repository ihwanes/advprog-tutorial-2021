# Tutorial 0
1. Membuat global repository untuk menyimpan seluruh tutorial Adpro semester ini, yaitu `advprog-tutorial-2021` dan clone repository utama `advprog-tutorial-2021` yang diberikan asdos
2. Membuat repo `the-art-of-git` berisi README.md untuk menyimpan Nama NPM Kelas, sempat clone repo kosong
3. Membuat repo `AP2021-Ihwanes` dalam folder tutorial0 dan kembali clone `advprog-tutorial-2021` sesuai petunjuk dan pemahaman akan soal
4. Mengerjakan soal branching `home, house, rumah` pada repository `AP2021-Ihwanes` di dalam folder advprog-tutorial-2021 yang terdapat di dalam folder tutorial0
