package id.ac.ui.cs.tutorial0.service;

import org.springframework.stereotype.Service;

import java.util.Calendar;

@Service
public class AdventurerCalculatorServiceImpl implements AdventurerCalculatorService {

    @Override
    public int countPowerPotensialFromBirthYear(int birthYear) {
        int rawAge = getRawAge(birthYear);
        int adventurePower;
        if (rawAge<30) {
            adventurePower = rawAge*2000;
        } else if (rawAge <50) {
            adventurePower = rawAge*2250;
        } else {
            adventurePower = rawAge*5000;
        }
        return adventurePower;
    }

    public String adventurerClass(int adventurePower){
        if (adventurePower <= 20000){
            return "C class adventurer with power " + adventurePower;
        } else if (adventurePower > 20000 && adventurePower <= 100000){
            return "B class adventurer with power " + adventurePower;
        } else {
            return "A class adventurer with power " + adventurePower;
        }
    }

    private int getRawAge(int birthYear) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        return currentYear-birthYear;
    }
}
